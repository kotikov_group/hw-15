const form = document.getElementById('myForm');

form.addEventListener("submit", () => {
    const inputHidden = form.querySelector('input[name=hiddenInput]');
    const submitButton = document.getElementById('submitBtn');

    let div = document.createElement('div');
    div.className = 'field-container';
    div.innerHTML = '<input type="hidden" name="newField" value="customValue" />';
    submitButton.before(div);

    let span = document.createElement('span');
    span.className = 'submitText';
    span.textContent = 'Форма отправлена!';
    submitButton.after(span);

    inputHidden.remove();
});
